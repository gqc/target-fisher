
# Target Fisher

Target Fisher is a Structure-Based Target Prediction tool that uses target-specific machine learning models, together with docking results to predict biological activies of small molecules.

It is also availble via a webserver [https://gqc.quimica.unlp.edu.ar/targetfisher/].

# Installation

Firstly, you need to install Open Babel, for which:

```bash
  apt install openbabel
```

Then, install the program directly from the repository:

```bash
  pip install git+https://gitlab.com/gqc/target-fisher.git
```
If this doesn't work or gives an error, try updating pip

```bash
  python3 -m pip install --upgrade pip
```

# Usage

This programm uses .smi, .mol2 or .sdf molecules files, so you'll need this file. You can also use the .pdbqt file format molecules.

Once you have this molecule file, you can run the programm. For that, simply type:

```bash
  targetfisher moleculefile jobname [options]
```
Note: moleculefile must have the extension.

Options:

- -np: Number of CPUs to use, if null targetfisher will use all. Number
- -t: Target. Must be written like Target_uniprotID, if non targetfisher will run on all the targets available. 

Example:

```bash
  targetfisher XYZ.mol2 example -np # -t BACE1_P56817
```

This will run the programm on XYZ.mol2 molecule using # cpus on the target BACE1. Results will be stored in a folder named 'example'.

# Cite us 

Soon
