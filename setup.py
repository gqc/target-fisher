from setuptools import setup, find_packages

setup(
    name="targetfisher",
    version="1.0",
    author="Julian Fernandez, Leandro Martinez Heredia, Martin Lavecchia",
    author_email="jffernandez1995@gmail.com, leandromartinezheredia@gmail.com, lavecchia@gmail.com",
    description="Target Fisher mixes Inverse Docking and Machine Learning for target prediction of small molecules.",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "Topic :: Scientific/Engineering :: Chemistry"
    ],
    install_requires=[
        "numpy",
        "tk",
        "matplotlib",
        "scipy",
        "pandas",
        "joblib",
        "reportlab",
        "scikit-learn",
        "rdkit",
        "meeko"
    ],
    entry_points={
        "console_scripts": [
            "targetfisher=targetfisher.fisher_vina:fisher_cli"
        ],
        "gui_scripts": [
            "targetfisher_gui=targetfisher.fisher_gui:main"
        ]
    },
    project_urls={
        "Homepage": "https://gitlab.com/gqc/target-fisher/",
        "Bug Tracker": "https://gitlab.com/gqc/target-fisher/-/issues"
    },
    packages=find_packages(),
    package_data={
        # If any package contains *.txt or *.rst files, include them:
        "": ["*.png"],
    }
)
