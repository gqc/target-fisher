from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Spacer, Image, Paragraph, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_CENTER
import pandas as pd
import os
from datetime import date
from targetfisher.config import config
inch = 70

class PDFReport:
    
    def __init__(self, jobname, pdf_filename):
        self.jobname = jobname
        self.pdf_filename = pdf_filename
        self.elements = []
        self.styles = getSampleStyleSheet()
        self.palette = {
            'dark_blue': colors.HexColor("#344055"),
            'medium_gray': colors.HexColor("#2d3e50"),
            'blue': colors.HexColor("#266dd3"),
            'dark_green': colors.HexColor("#1e555c"),
            'light_green': colors.HexColor("#89baac"),
            'light_gray': colors.HexColor("#95a5a6"),
            'lighter_gray': colors.HexColor("#ecf0f1"),
        }
        self.git_repo_link = "https://gitlab.com/gqc/target-fisher"
        self.footer_text = "Generated by Target Fisher " + str(date.today())
        self.footer = Paragraph(self.footer_text + " " + self.git_repo_link)

    def create_report(self):
        self.first_page()
        self.other_sections()
        self.build_pdf()

    def first_page(self):
        img = Image('./utils/gqclogo.png', kind='proportional')
        img.drawHeight = 0.7*inch
        img.drawWidth = 2.1*inch
        img.hAlign = 'LEFT'
        self.elements.append(img)

        spacer = Spacer(30, 150)
        self.elements.append(spacer)

        img = Image('./utils/logo.png')
        img.drawHeight = 2*inch
        img.drawWidth = 6*inch
        self.elements.append(img)

        spacer = Spacer(10, 150)
        self.elements.append(spacer)

        psDetalle = ParagraphStyle('Resumen', fontSize=9, leading=14, justifyBreaks=1, alignment=TA_LEFT, justifyLastLine=1)
        text = """REPORT<br/>
        Submission: """+self.jobname+"""<br/>
        Date: """+str(date.today())+"""<br/>
        """
        mol_png = None
        for f in os.listdir('./results/'+self.jobname):
            if f.endswith('.png'):
                mol_png = './results/'+self.jobname+'/'+f
        
        # Create a container for the image and summary paragraph
        data = [
            [Paragraph(text, psDetalle), Image(mol_png, kind='proportional', width=1.7*inch, height=1.7*inch, hAlign='RIGHT')]
        ]
        page_width, page_height = letter
        # Create a table to hold the image and summary paragraph
        table = Table(data,  colWidths=[page_width - 5*inch, 2*inch])

        # Append the table to self.elements
        self.elements.append(table)
        self.elements.append(PageBreak())

    def other_sections(self):
        self.predictions_table_maker()
        self.per_target_results()

    def predictions_table_maker(self):        
        psHeaderText = ParagraphStyle('Hed0', fontSize=12, alignment=TA_LEFT, borderWidth=8, textColor=self.palette['dark_blue'])\
        
        dfs = []
        files = os.listdir('./results/'+self.jobname+'/scores/')
        for csv in sorted(files):
            if csv.endswith('results.csv'):
                df = pd.read_csv('./results/'+self.jobname+'/scores/'+csv)
                print(csv)
                dfs.append(df)
        dff = pd.concat(dfs).drop(['Unnamed: 0'], axis=1) 
        
        text = 'PREDICTIONS'
        paragraphReportHeader = Paragraph(text, psHeaderText)
        self.elements.append(paragraphReportHeader)

        spacer = Spacer(10, 22)
        self.elements.append(spacer)
        """
        Create the line items
        """
        d = []
        columns = dff.columns
                
        fontSize = 8
        centered = ParagraphStyle(name="centered", alignment=TA_CENTER)
        

        
        for text in columns:
            ptext = "<font size='%s'><b>%s</b></font>" % (fontSize, text)
            titlesTable = Paragraph(ptext, centered)
            d.append(titlesTable)        

        data = [d]
        lineNum = 1
        formattedLineData = []

        alignStyle = [ParagraphStyle(name="01", alignment=TA_CENTER),
                      ParagraphStyle(name="02", alignment=TA_CENTER),
                      ParagraphStyle(name="03", alignment=TA_CENTER),
                      ParagraphStyle(name="04", alignment=TA_CENTER),
                      ParagraphStyle(name="05", alignment=TA_CENTER)]
 
        data_0 = dff.values.tolist()
        for row in data_0:
            #print(row)
            try:
                lineData = row
                columnNumber = 0
                for item in lineData:
                    
                    ptext = "<font size='%s'>%s</font>" % (fontSize, item)
                    p = Paragraph(ptext, alignStyle[columnNumber])
                    formattedLineData.append(p)
                    columnNumber = columnNumber + 1
                data.append(formattedLineData)
                formattedLineData = []
            except Exception as e:
                print(row, e)
        table = Table(data, colWidths=[50, 200, 80, 80, 80])
        table.setStyle(TableStyle([
            ('ALIGN', (0, 0), (0, -1), 'LEFT'),
            ("ALIGN", (1, 0), (1, -1), 'RIGHT'),
            ('BACKGROUND', (0, 0), (-1, 0), colors.HexColor("#328c8c")),
            ('TEXTCOLOR', (0, 0), (-1, 0), self.palette['light_gray']),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('LINEABOVE', (0, 0), (-1, -1), 1, colors.HexColor("#7ab3b3")),
            ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
            #('BACKGROUND', (0, 1), (-1, -1), colors.HexColor("#7ab3b3")),
            #('GRID', (0, 0), (-1, -1), 1, self.palette['medium_gray'])
        ]))
        for i in range(1, len(data_0)):
            if dff.iloc[i - 1]['Result'] == 'Potentially Active':
                bg_color = self.palette['blue']
            elif dff.iloc[i - 1]['Result'] == 'Inconclusive':
                bg_color = 'white'
            elif dff.iloc[i - 1]['Result'] == 'Potentially Inactive':
                bg_color = self.palette['lighter_gray']
            table.setStyle(TableStyle([('BACKGROUND', (0, i), (-1, i), bg_color)]))
        
        #table.setStyle(tStyle)

        self.elements.append(table)
        # Add Footer

        self.elements.append(self.footer)
        self.elements.append(PageBreak())
        
    def per_target_results(self):
        psHeaderText = ParagraphStyle('Hed0', fontSize=12, alignment=TA_LEFT, borderWidth=3, textColor=self.palette['dark_blue'])\
        
        text = 'PER TARGET RESULTS'
        paragraphReportHeader = Paragraph(text, psHeaderText)
        self.elements.append(paragraphReportHeader)
        spacer = Spacer(10, 22)
        self.elements.append(spacer)
        imgs = []
        for img in os.listdir('./results/'+self.jobname+'/scores/'):
            if img.endswith('.png'):
                imgs.append('./results/'+self.jobname+'/scores/'+img)
            # Add images to the PDF
        i = 0
        for path in sorted(imgs):
            self.elements.append(Image(path, width=360, height=270))
            self.elements.append(Spacer(1, 12))
            i = i + 1
            if i % 2 == 0:
                 self.elements.append(self.footer)
            else:
                pass
    def build_pdf(self):
        doc = SimpleDocTemplate(self.pdf_filename, pagesize=letter)
        doc.build(self.elements)


