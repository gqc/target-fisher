import os
import sys
from targetfisher.config import config
import subprocess
from shutil import copyfile

def exists_infile_and_outdir(infile, outdir):
    if not os.path.isfile(infile):
        raise RuntimeError(f"Inputfile {infile} does not exist.")
    if not os.path.isdir(outdir):
        raise RuntimeError(f"Output directory {outdir} does not exist.")

def get_best_score(vina_output):
    with open(vina_output) as vina_output_file:
        for line in vina_output_file:
            if line.startswith("   1"):
                return float(line.strip().split()[1])

def convert_to_pdbqt(moleculefile, outdir):
    moleculedir, moleculefilename = os.path.split(moleculefile)
    molbasename, mol_extension = os.path.splitext(moleculefilename)
    file_path = outdir + '/' + moleculefilename
    try:
        copyfile(moleculefile, file_path)
    except FileNotFoundError:
        print("File not found:", file_path)
        return
    # ~ os.system(f'obabel -i{mol_extension.replace(".","")} {file_path} --gen2d -osdf > {outdir}/{molbasename}_2d.sdf')
    os.system(f"python3 ./utils/generate_images.py {molbasename} {mol_extension} {outdir}")
    if mol_extension == '.mol2':
        print('Detected .mol2. Running mk_prepare_ligand.py')
        try:
            os.system(f'mk_prepare_ligand.py -i {outdir}/{molbasename+mol_extension} -o {outdir}/ligands/{molbasename}.pdbqt')
        except Exception as e:
            print(f'Problem {e} in file {moleculefile}')
            sys.exit()

    elif mol_extension == '.sdf':
        print('Detected .sdf.  Running mk_prepare_ligand.py')
        try:
            os.system(f'mk_prepare_ligand.py -i {outdir}/{molbasename+mol_extension} -o {outdir}/ligands/{molbasename}.pdbqt')
        except Exception as e:
            print(f'Problem {e} in file {moleculefile}')

    elif mol_extension == '.smi':
        print('Detected .smi, converting to .sdf OBabel Gen3D.')
        os.system(f'obabel -ismi {file_path} --gen3d -osdf > {outdir}/{molbasename}.sdf')
        try:
            os.system(f'mk_prepare_ligand.py -i {outdir}/{molbasename}.sdf -o {outdir}/ligands/{molbasename}.pdbqt')
        except Exception as e:
            print(f'Problem {e} in file {moleculefile}')
            sys.exit()
        print('Success')
    else:
        print('Input format error')
