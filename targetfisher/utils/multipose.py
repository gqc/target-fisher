import os
import pandas as pd

def get_poses(outpdbqt, cutoff_score, cutoff_rmsd):
    with open(outpdbqt, 'r+') as f:
        try:
            scores = []
            pose_data = []
            for line in f:
                if line.startswith('REMARK VINA RESULT:'):
                    score = line.split('RESULT:')[1].split()[0]
                    rmsd = line.split('RESULT:')[1].split()[1]
                    scores.append(score)
                    pose_data.append([score,rmsd])
            selected_poses = [1]
            for score, rmsd in pose_data:
                cutoff = cutoff_score
                # ~ scores.append(final_poses[0][0][0])
                if float(score) < float(scores[0])*cutoff:
                    if float(rmsd) > cutoff_rmsd:
                        #print(pose_data.index([score, rmsd]))
                        selected_poses.append(pose_data.index([score, rmsd])+1)
                else:
                    pass
        except Exception as e:
            print(e)
    f.close()
    return selected_poses, scores[0:len(selected_poses)]
