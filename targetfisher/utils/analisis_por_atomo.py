'''
analisis_por_atomo.py archivo.mol2 ligandoid output_path
permite extraer una lista de interaccion con el ligando 
'''

#! /usr/bin/python3.8
import pandas as pd
import sys
import subprocess
import os
from targetfisher.config import config

def analisis_por_atomo(molfile_name, ligandid, forcefield, output_path):

    # LECTURA MOL2
    molfile = open(molfile_name,"r")
    molfile_lines = molfile.readlines()
    molfile_start_index = [i for i, s in enumerate(molfile_lines) if '@<TRIPOS>ATOM' in s][0]+1
    try:
        molfile_end_index = [i for i, s in enumerate(molfile_lines) if '@<TRIPOS>UNITY_ATOM_ATTR' in s][0]
    except:
        molfile_end_index = [i for i, s in enumerate(molfile_lines) if '@<TRIPOS>BOND' in s][0]

    # arma un diccionario con los id de los residuos y los atomos de cada uno.
    residue_dic = {}
    for i in range(molfile_start_index,molfile_end_index):
        # 1 O           1.6586    2.0599    2.2863 O.3     1  HOH1       -0.4105
        molfile_lines_div = molfile_lines[i].split()
        # ~ print(molfile_lines_div)
        atomid = int(molfile_lines_div[0])
        resid = int(molfile_lines_div[6])  
        # ~ print(molfile_lines_div)
        if resid in residue_dic:
            residue_dic[resid].append(atomid)
        else:
            residue_dic[resid]=[atomid]
    # ~ print(residue_dic)
    if ligandid is None:
        ligandid = resid # last residue

    # ANALISIS OBENERGY 
    # Defined Mygaff and Mymmff94 forcefields, which return atom index instead of type
    print(config.PACKAGE_DIR + '/mm_fingerprints/obenergy', '-ff', f"My{forcefield}", "-l", f"{ligandid}", '-v', molfile_name)
    result = subprocess.run([config.PACKAGE_DIR + '/mm_fingerprints/obenergy', '-ff', f"My{forcefield}", "-l", f"{ligandid}", '-v', molfile_name ], stdout=subprocess.PIPE)

    # ~ result = subprocess.run(['obenergy', '-ff', f"My{forcefield}", "-l", f"{ligandid}", '-v', molfile_name ], stdout=subprocess.PIPE)

    oboutput = result.stdout.decode('ascii').splitlines()
    # ~ print(result)


    #
    # electrostatic energy
    #
    for i, s in enumerate(oboutput):
        if 'E L E C T R O S T A T I C   I N T E R A C T I O N S' in s:
            electrostatics_start_index = i + 5
            break

    # ~ electrostatics_start_index = [i for i, s in enumerate(oboutput) if 'E L E C T R O S T A T I C   I N T E R A C T I O N S' in s][0]+5
    for i, s in enumerate(oboutput):
        if 'TOTAL ELECTROSTATIC ENERGY' in s:
            electrostatics_end_index = i
            break

    atom_electrostatics_dic = {} #store total electrostatic energy by residues atom with the ligand atoms
    for i in range(electrostatics_start_index, electrostatics_end_index):
        if forcefield=="gaff":
            # I    J           Rij   332.17*QiQj  ENERGY
            oboutputdiv = oboutput[i].split()
            id_atomi = int(oboutputdiv[0])
            id_atomj = int(oboutputdiv[1])
            energyij = float(oboutputdiv[4].strip())
        
        elif forcefield=="mmff94":
            #I    J        Rij        Qi         Qj        ENERGY
            oboutputdiv = oboutput[i].split()
            id_atomi = int(oboutputdiv[0])
            id_atomj = int(oboutputdiv[1])
            energyij = float(oboutputdiv[5].strip())
        else:
            print("You shuld define a forcefield: gaff, mmff94")
            exit()

        if (id_atomi in residue_dic[ligandid]) and (id_atomj not in residue_dic[ligandid]):
            if id_atomj in atom_electrostatics_dic:
                atom_electrostatics_dic[id_atomj]+= energyij
            else:
                atom_electrostatics_dic[id_atomj]= energyij
        elif (id_atomj in residue_dic[ligandid]) and (id_atomi not in residue_dic[ligandid]):
            if id_atomi in atom_electrostatics_dic:
                atom_electrostatics_dic[id_atomi]+= energyij
            else:
                atom_electrostatics_dic[id_atomi]= energyij

    residue_electrostatics_dic = {} #store total electrostatic energy by residues with the ligand
    for resid in residue_dic:
        if resid != ligandid:
            residue_electrostatics_dic[resid]=0
            for atomid in residue_dic[resid]:
                try:
                    residue_electrostatics_dic[resid]+= atom_electrostatics_dic[atomid]
                except:
                    pass

    #
    # van der Waals energy
    #
    vdw_start_index = [i for i, s in enumerate(oboutput) if 'V A N   D E R   W A A L S' in s][0]+5
    vdw_end_index = [i for i, s in enumerate(oboutput) if 'TOTAL VAN DER WAALS ENERGY' in s][0]

    atom_vdw_dic = {} #store total vdw energy by residues atom with the ligand atoms
    for i in range(vdw_start_index, vdw_end_index):
        if forcefield=="gaff":
            # I    J           Rij   332.17*QiQj  ENERGY
            oboutputdiv = oboutput[i].split()
            id_atomi = int(oboutputdiv[0])
            id_atomj = int(oboutputdiv[1])
            energyij = float(oboutputdiv[3].strip())
        
        elif forcefield=="mmff94":
            #I    J        Rij        Qi         Qj        ENERGY
            oboutputdiv = oboutput[i].split()
            id_atomi = int(oboutputdiv[0])
            id_atomj = int(oboutputdiv[1])
            energyij = float(oboutputdiv[5].strip())
        else:
            print("You should define a forcefild: gaff, mmff94")
            exit()

        if (id_atomi in residue_dic[ligandid] and id_atomj not in residue_dic[ligandid]):
            if id_atomj in atom_vdw_dic:
                atom_vdw_dic[id_atomj]+= energyij
            else:
                atom_vdw_dic[id_atomj]= energyij

        elif (id_atomj in residue_dic[ligandid] and id_atomi not in residue_dic[ligandid]):
            if id_atomi in atom_vdw_dic:
                atom_vdw_dic[id_atomi]+= energyij
            else:
                atom_vdw_dic[id_atomi]= energyij

    residue_vdw_dic = {} #store total electrostatic energy by residues with the ligand
    for resid in residue_dic:
        if resid != ligandid:
            residue_vdw_dic[resid]=0
            for atomid in residue_dic[resid]:
                try:
                    residue_vdw_dic[resid]+= atom_vdw_dic[atomid]
                except:
                    pass

    # ~ print(residue_electrostatics_dic)
    # ~ print(residue_vdw_dic)

    df_es = pd.DataFrame.from_dict(residue_electrostatics_dic,  orient="index")
    df_es_t = df_es.transpose()
    df_es_t['Energy'] = 'es'
    # ~ df_es_t.to_csv('./es/'+molfile_name.split('/')[-1].replace('.mol2','')+'_fp_es.csv')

    df_wdw = pd.DataFrame.from_dict(residue_vdw_dic, orient="index")
    df_wdw_t = df_wdw.transpose()
    df_wdw_t['Energy'] = 'vdw'
    # ~ df_wdw_t.to_csv('./vdw/'+molfile_name.split('/')[-1].replace('.mol2','')+'_fp_vdw.csv')

    df_full = pd.concat([df_es_t, df_wdw_t])
    # ~ df_full.loc['Total',:] = df_full.sum(axis=0) 
    df_full['Ligand'] = molfile_name.split('/')[-1].split('.')[0]
    df_full.to_csv(output_path+molfile_name.split('/')[-1].replace('.mol2','')+'_fp_full.csv', index = False)
    # ~ print(residue_electrostatics_dic)
    print('Fingerprint generated : '+ molfile_name.split('/')[-1].replace('.mol2',''))
    # ~ print(residue_vdw_dic)

if __name__ == "__main__":
    molfile_name = sys.argv[1]
    ligandid = int(sys.argv[2])
    forcefield = "mmff94"   #implementado gaff or mmff94 
    output_path = sys.argv[3]
    analisis_por_atomo(molfile_name, ligandid, forcefield, output_path)