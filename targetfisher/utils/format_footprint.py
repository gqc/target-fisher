import os
import re
from targetfisher.config import config

def get_res(rec):
	filein = open(config.VINA_DOCKING_PATH+rec+'/prep_receptor.mol2','r+')
	lines = filein.readlines()
	n = 0
	res_list = []
	for line in lines:
		if line.startswith('@<TRIPOS>ATOM'):
			n = 1
			continue
		if line.startswith('@<TRIPOS>BOND'):
			n = 2
			break
		if n == 1:
			try:
				if re.search(r'\d+', line.split()[7]) is None:
					res = line.split()[7]+line.split()[6]
				else:
					res = line.split()[7]
				if res not in res_list:
					res_list.append(res)
			except Exception as e:
				print(e)
	res_list.append('Ligand')
	filein.close()
	return res_list

