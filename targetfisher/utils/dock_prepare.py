## Scripts for preparing DOCK6.9 input files

import pandas as pd
import os
import sys
from targetfisher.config import config as tfconfig

def generate_run_vina(path, vina_docking_path, target_list, jobname, np):
    with open(path+'/docking/run_vina.sh','w+') as vina_run:
        for ligand in os.listdir(path+'/ligands'):
            if ligand.endswith('.pdbqt'):
                print('Found ligand')
                for rec in target_list:
                    if os.path.isdir(vina_docking_path+'/'+rec):
                        for config in os.listdir(vina_docking_path+'/'+rec):
                            if config == 'config.txt':
                                vina_run.write(f'{tfconfig.VINA_PATH} --receptor '+vina_docking_path+rec+'/prep_receptor_adt.pdbqt'
                                                   ' --ligand ./results/'+jobname+'/ligands/'+ligand+ 
                                                   ' --cpu '+str(np)+
                                                   ' --config '+vina_docking_path+rec+'/config.txt'
                                                   # ~ ' --log ./results/'+jobname+'/docking/'+rec+'/'+ligand.split('.pdbqt')[0]+'.log'+
                                                   ' --out ./results/'+jobname+'/docking/'+rec+'/'+ligand.split('.pdbqt')[0]+'_out.pdbqt > ./results/'+jobname+'/docking/'+rec+'/'+ligand.split('.pdbqt')[0]+'.log ;\n')
        vina_run.close()
        print('Vina run file generated')
