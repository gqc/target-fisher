import os
import pandas as pd
from . import multipose
from . import perform_prediction as pf
from . import format_footprint
import re
import matplotlib.patches as mpatches
import matplotlib.cm as cm
import numpy as np
import matplotlib.pyplot as plt
import subprocess
from targetfisher.config import config

def process_docking_results(jobname, vina_docking_path, score_threshold, rmsd_threshold):
    logf = open('./results/'+jobname+'/'+jobname+'_fisher.log', 'w+')
    for rec in os.listdir(f'./results/{jobname}/docking'):
        if os.path.isdir(f'./results/{jobname}/docking/{rec}'):
            res_ls = []
            if os.path.exists(f'{config.PACKAGE_DIR}/vina/docking/{rec}/prep_receptor.mol2'):
                res_ls = format_footprint.get_res(rec)
                print(rec)

            for ligand in os.listdir(f'./results/{jobname}/docking/{rec}'):
                if ligand.endswith('_out.pdbqt'):
                    try:
                        subprocess.run(f'mk_export.py ./results/{jobname}/docking/{rec}/{ligand} -o ./results/{jobname}/docking/{rec}/{ligand.replace(".pdbqt","")}.sdf'.split())
                        subprocess.run(f'obabel -isdf ./results/{jobname}/docking/{rec}/{ligand.replace(".pdbqt","")}.sdf -O ./results/{jobname}/docking/{rec}/{ligand.replace(".pdbqt","")}.mol2'.split())
                        subprocess.run(f'obabel -imol2 ./results/{jobname}/docking/{rec}/{ligand.replace(".pdbqt","")}.mol2 -m -O ./results/{jobname}/docking/{rec}/{ligand.replace(".pdbqt","")}_pose.mol2'.split())
                        selected_poses, scores = multipose.get_poses(f'./results/{jobname}/docking/{rec}/{ligand}', score_threshold, rmsd_threshold)
                    except Exception as e:
                        print(e)

            for ligand in os.listdir(f'./results/{jobname}/docking/{rec}'):
                for pose in selected_poses:
                    if ligand.endswith(str(pose) + '.mol2'):
                        print(pose)
                        last_res = os.popen(f'python3 {config.PACKAGE_DIR}/utils/merge_mol2.py {vina_docking_path}{rec}/prep_receptor.mol2 ./results/{jobname}/docking/{rec}/{ligand.split("_pose")[0]}_pose{str(pose)}.mol2 ./results/{jobname}/docking/{rec}/{ligand.split("_pose")[0]}_pose{str(pose)}_complex.mol2').read()
                        # ~ print(f'python ./utils/merge_mol2.py {vina_docking_path}{rec}/prep_receptor.mol2 ./results/{jobname}/docking/{rec}/{ligand.split("_pose")[0]}_pose{str(pose)}_h.mol2 ./results/{jobname}/docking/{rec}/{ligand.split("_pose")[0]}_pose{str(pose)}_complex.mol2').read()
                        ligandid = int(last_res.split('\n')[0])   
                        subprocess.run(f'python3 {config.PACKAGE_DIR}/utils/analisis_por_atomo.py ./results/{jobname}/docking/{rec}/{ligand.split("_pose")[0]}_pose{str(pose)}_complex.mol2 {str(ligandid)} ./results/{jobname}/docking/{rec}/'.split())
                        logf.write(f'Complex {rec} {ligand} ok \n')

            print('Processing Footprints')
            rs = []
            for f in os.listdir(f'./results/{jobname}/docking/{rec}'):
                if f.endswith('_full.csv'):
                    ligand_name = f.split('_')[0] + '_' + f.split('_')[2]
                    print(ligand_name)
                    df = pd.read_csv(f'./results/{jobname}/docking/{rec}/{f}', index_col=False)
                    df.loc[f.split('_')[0], :] = df.sum(axis=0)
                    df = df.drop([0, 1])
                    df = df.drop(['Energy'], axis=1)
                    try:
                        df.columns = res_ls
                    except Exception as e:
                        print(e)
                        logf.write(f'Error {e} \n')
                    final_fingerprint_filename = f'./results/{jobname}/docking/{rec}/{jobname}_{rec.split("_")[0]}_{f.split("_")[2]}_fingerprint.csv'
                    df.drop(['Ligand'], axis=1).to_csv(final_fingerprint_filename)
                    print('Making Predictions')  # Perform predictions
                    try:
                        pred = pf.perform_prediction(final_fingerprint_filename, f'{config.PACKAGE_DIR}/vina/models', jobname, ligand_name, rec)
                        # ~ print(scores)
                        print(int(f.split('pose')[1][0]) - 1)
                        vina_score = scores[(int(f.split('pose')[1][0]) - 1)]
                        print(vina_score)
                        # ~ df_m = pd.read_csv('./vina/metrics.csv')
                        # ~ score_consenso, score_vina_aass, score_ml_aass, result = metrics.results(scores[(int(f.split('_')[2][-1]) - 1)], pred, rec, df_m)
                        result = results(rec, float(vina_score), float(pred))
                        r = pd.DataFrame({"Molecule": [str(ligand_name)], "Target": [rec], "Vina_Score": [vina_score], "Prediction": [round(pred,2)], "Result": [result]})
                        rs.append(r)
                        print(r)
                        make_graphic(jobname, rec, float(vina_score) ,float(pred))
                        
                    except Exception as e:
                        logf.write(f'Error {e} while making prediction in: {rec} {final_fingerprint_filename} \n')
            pd.concat(rs).to_csv(f'./results/{jobname}/scores/{jobname}_{rec}_results.csv')
    logf.close()

def results(target, vina_score, ml_score):
    merge_df = pd.read_csv(f'{config.PACKAGE_DIR}/vina/models/'+target+'/dockingml_test_set_1_15.csv')
    if merge_df['Prediction'].mean() >= 0.5:
        mean_prediction = merge_df['Prediction'].mean() 
    else:
        mean_prediction = 0.5
    mean_score = merge_df['Score'].mean()
    if vina_score < mean_score and ml_score > mean_prediction and ml_score > 0.5:
        result = 'Potentially Active'
    elif vina_score > mean_score and ml_score < mean_prediction:
        result = 'Potentially Inactive'
    else:
        result = 'Inconclusive'
    return result



def make_graphic(jobname, target, score_vina, score_ml):
    merge_df = pd.read_csv(f'{config.PACKAGE_DIR}/vina/models/'+target+'/dockingml_test_set_1_15.csv')
    if merge_df['Prediction'].mean() >= 0.5:
        mean_prediction = merge_df['Prediction'].mean() 
    else:
        mean_prediction = 0.5
    mean_score = merge_df['Score'].mean()
    colors = merge_df['Activity_x'].map({0: 'darkred', 1: 'darkgreen'})
    # Create custom legend handles and labels for 'Activity_x'
    legend_handles = [mpatches.Patch(color='darkred', label='Inactive Compounds'),
                      mpatches.Patch(color='darkgreen', label='Active Compounds')]
                      
    fig, ax = plt.subplots(figsize=(8, 6))

    ax.scatter(merge_df['Prediction'], merge_df['Score'], c=colors, alpha=0.4)
    ax.scatter(score_ml, score_vina, color='red', marker='X', s=150, label=jobname)
    ax.axvline(x=mean_prediction, color='black', linestyle='--', label='Mean')
    ax.axhline(y=mean_score, color='black', linestyle='--')
    # Set limits for the initial scatter plot
    # ~ ax.set_xlim(merge_df['Prediction'].min(), merge_df['Prediction'].max())
    # ~ ax.set_ylim(merge_df['Score'].min(), merge_df['Score'].max())    
    ax.set_title(jobname+' '+target)
    legend_handles.append(ax.scatter(score_ml, score_vina, color='blue', marker='X', s=150, label=jobname))
    # Set labels and title
    ax.set_xlabel('Prediction using the ML model')
    ax.set_ylabel('Docking Score')
    ax.invert_yaxis()

    legend_handles.append(ax.axvline(x=mean_prediction, color='black', linestyle='--', label='Mean Prediction'))
    legend_handles.append(ax.axhline(y=mean_score, color='black', linestyle='--', label='Mean Score'))
    fig.legend(handles=legend_handles, bbox_to_anchor=(1, 1), loc='upper right')

    fig.savefig(f'./results/{jobname}/scores/{jobname}_{target}.png', bbox_inches='tight')
    print('Graphic saved')

