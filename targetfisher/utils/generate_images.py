import os
import sys
from targetfisher.config import config
import subprocess
from shutil import copyfile
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import Draw as rdkitdraw

mol_basename = sys.argv[1]
mol_extension = sys.argv[2]
outdir = sys.argv[3]

def create_mol_figures(mol_basename, mol_extension, outdir):
    os.system(f"obabel -i{mol_extension.replace('.','')} {outdir}/{mol_basename+mol_extension} -osmi > {outdir}/{mol_basename}.smi")
    with open(f"{outdir}/{mol_basename}.smi", 'r+') as f:
        lines = f.readlines()
        mol = Chem.MolFromSmiles(lines[0])
        compound_image = rdkitdraw.MolToFile(mol, f"{outdir}/{mol_basename}.png")
    f.close()

create_mol_figures(mol_basename, mol_extension, outdir)
