#!/usr/bin/env python
# coding: utf-8

import os
import sys

def merge_mol2(receptor_filename, ligand_filename, complex_filename):
    with open(complex_filename, "w+") as complex_file:
        ## Add @<TRIPOS>MOLECULE

        complex_file.write('@<TRIPOS>MOLECULE\n')
        complex_file.write(f'{complex_filename}\n')

        ## Agrego Linea con Numero de Atomos y Enlaces

        line_complex_file = [0,0,str(0),str(0),str(0)]
        with open(receptor_filename, 'r') as rec:
            for i, line in enumerate(rec):
                if i == 2:
                    # ~ print(line)
                    line_rec = line.split()
        with open(ligand_filename, 'r') as lig:
            for j, line in enumerate(lig):
                if j == 2:
                    # ~ print(line)
                    line_lig = line.split()
        line_complex_file[0] = str(int(line_rec[0]) + int(line_lig[0]))
        line_complex_file[1] = str(int(line_rec[1]) + int(line_lig[1]))
        complex_file.write(' '.join(line_complex_file)+'\n')

        complex_file.write('SMALL'+'\n')
        complex_file.write('GASTEIGER'+'\n')
        complex_file.write('\n')

        ## Leo el Receptor y guardo el ultimo numero de atomo

        newlines = []
        printLines = False
        with open(receptor_filename, 'r') as rec:
            for i, line in enumerate(rec):
                line = line.strip('\n')
                if line.startswith('@<TRIPOS>ATOM'):
                    line = line.strip('\n')
                    printLines = True
                if line.startswith('@<TRIPOS>BOND'):
                    printLines = False
                if printLines:
                    line_s_rec = line.split()
                    complex_file.write(' '.join(line_s_rec)+'\n')
                    newlines.append(line)

        import re
        last_atom = re.findall('\d+', newlines[-1])[0]
        last_res = newlines[-1].split()[-3]
        last_res_corr = int(last_res)+1
        print(int(last_res)+1)

        printLines = False
        value = 0
        with open(ligand_filename, 'r') as lig:
            for line in lig:
                line = line.strip('\n')
                if line.startswith('@<TRIPOS>ATOM'):
                    printLines = True
                    value = value + 1
                if line.startswith('@<TRIPOS>BOND'):
                    printLines = False
                if printLines and value == 1:
        #             print(line_s)
                    try:
                        line_s = line.split()
                        line_s[0] = str(int(line_s[0]) + int(last_atom))
                        line_s[6] = str(int(last_res) + 1)
        #                 print(line_s)
                    except:
                        pass
                    if not line.startswith('@<TRIPOS>ATOM'):
                        complex_file.write(' '.join(line_s)+'\n')

        ## <TRIPOS>BOND Receptor

        # complex_file = open('actives1_complex_filelex.mol2', 'w+')
        printLines = False
        bond_lines = []
        value == 0
        with open(receptor_filename, 'r') as rec:
            for line in rec:
                line = line.strip('\n')
                if line.startswith('@<TRIPOS>BOND'):
                    printLines = True
                if line.startswith('@<TRIPOS>SUBSTRUCTURE'):
                    printLines = False
                if printLines:
                    line_s_rec = line.split()
                    complex_file.write(' '.join(line_s_rec)+'\n')
                    bond_lines.append(line)
        # ~ last_bond = re.findall('\d+', bond_lines[-1])[0]

        ## <TRIPOS>BOND Ligand

        newlines = []
        printLines = False
        value = 0
        with open(ligand_filename, 'r') as lig:
            for line in lig:
                line = line.strip('\n')
                if line.startswith('@<TRIPOS>BOND'):
                    value = value +1
                    printLines = True
                if printLines and value == 1:
        #             print(line)
                    if not line.startswith('@<TRIPOS>BOND') and not line.startswith('@<TRIPOS>MOLECULE'):
                        try:
                            line_s = line.split()
                            for i in range(1,3):
                                try:
                                    line_s[i] = str(int(line_s[i]) + int(last_atom))
                                    line_s[0] = str(int(line_s[0]) + int(last_bond))
                                except:
                                    pass
        #                     print(line)
                            # ~ print(line_s)
                            complex_file.write(' '.join(line_s)+'\n')
                        except Exception as e:
                            print(e)
                            pass
                if line.startswith('@<TRIPOS>MOLECULE'):
                    printLines = False
        print('Finished '+ complex_filename)
        return last_res_corr

if __name__ == "__main__":
    receptor_filename = sys.argv[1]
    ligand_filename = sys.argv[2]
    complex_filename = sys.argv[3]
    merge_mol2(receptor_filename, ligand_filename, complex_filename)