import os
import argparse
import pandas as pd
import joblib

def perform_prediction(fingerprint_file, models_path, jobname, ligand, target):
    pipe = None
    for model in os.listdir(models_path+'/'+target+'/'):
        if model.endswith('.pkl'):
            pkl_model = model
            pipe = joblib.load(models_path+'/'+target+'/'+model)
    if pipe is None:
        raise ValueError(f"No model for target {target}")
    ps = pd.read_csv(fingerprint_file, header=0)
    pred_cols = list(ps.columns.values)[1:]
    pred_cols = pipe.named_steps['preprocessing'].transformers[0][2]
    # ~ pred_cols = pipe.named_steps['preprocessing'].transformers[0][2]
    pred = pd.Series(pipe.predict_proba(ps[pred_cols])[:,1])
    ps['prediction'] = pd.Series(pipe.predict_proba(ps[pred_cols])[:,1])
    ps['target'] = target
    ps['molecule'] = ps.iloc[:, [0]]
    predicted_df = ps[['molecule', 'target', 'prediction']].copy()
    # ~ predicted_df.to_csv('./results/'+jobname+'/predictions/'+jobname+'_'+target.split('_')[0]+'_'+ligand+'_predictions.csv')
    return pred.values[0]
