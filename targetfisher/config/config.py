import os

PACKAGE_DIR = os.path.dirname(os.path.dirname(__file__))

## Define Paths
VINA_PATH = os.getenv("TFISHER_VINA_PATH", default=PACKAGE_DIR + '/utils/vina')


VINA_DOCKING_PATH = os.getenv("TFISHER_VINA_DOCKING_PATH", default=PACKAGE_DIR + '/vina/docking/')
VINA_MODELS_PATH =os.getenv("TFISHER_VINA_MODELS_PATH", default= PACKAGE_DIR + '/vina/models/')

