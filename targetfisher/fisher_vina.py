#!/usr/bin/env python
# coding: utf-8
## Usage: python3 fisher.py molecules.xxx jobname doc_soft nprocs / 'xxx' can be rather .smi or .mol2; doc_soft can be 'dock69' or 'vina'

import sys
import os
import pathlib
import argparse
from targetfisher.utils import ligand_tools as ltools
from targetfisher.utils import dock_prepare as dp
from targetfisher.utils import perform_prediction as pf
from targetfisher.utils import dock_prepare as dp
from targetfisher.utils import process_results
from targetfisher.utils.report import PDFReport
from targetfisher.utils import format_footprint
from targetfisher.utils.merge_mol2 import merge_mol2
from targetfisher.utils.analisis_por_atomo import analisis_por_atomo
from targetfisher.config import config
from datetime import date
import shutil
import pandas as pd
import joblib

def is_valid_target(t):
    target_docking = config.VINA_DOCKING_PATH + t
    target_result  = config.VINA_MODELS_PATH + t

    target_docking_files = os.listdir(target_docking)
    target_result_files = os.listdir(target_result)

    for required_docking_file in "config.txt prep_receptor_adt.pdbqt prep_receptor.mol2".split():
        if not required_docking_file in target_docking_files:
            return False

    for result_file in target_result_files:
        if result_file.endswith("pkl"):
            return True
    return False

def get_valid_targets():
    target_docking = config.VINA_DOCKING_PATH
    target_result  = config.VINA_MODELS_PATH

    target_docking_files = os.listdir(target_docking)
    target_result_files = os.listdir(target_result)

    target_candidates = set(target_docking_files) & set(target_result_files)
    valid_targets = [target for target in target_candidates if is_valid_target(target)]

    return valid_targets

def create_folder_structure(results_dir):
    if os.path.exists(results_dir):
        print('Job exists! Overwritting')
        shutil.rmtree(results_dir)
    os.makedirs(results_dir, exist_ok=True)

def main(moleculefile, jobname, np=None, t=None):
    print("""
   _____                    _    ______ _     _               
  |_   _|                  | |   |  ___(_)   | |              
    | | __ _ _ __ __ _  ___| |_  | |_   _ ___| |__   ___ _ __ 
    | |/ _` | '__/ _` |/ _ | __| |  _| | / __| '_ \ / _ | '__|
    | | (_| | | | (_| |  __| |_  | |   | \__ | | | |  __| |   
    \_/\__,_|_|  \__, |\___|\__| \_|   |_|___|_| |_|\___|_|   
                  __/ |                                       
                 |___/                                        
    """)

    if not os.path.isfile(moleculefile):
        raise ValueError(f"Molecule file {moleculefile} is not a file.")

    moleculedir, moleculefilename = os.path.split(moleculefile)
    filein, file_extension = os.path.splitext(moleculefilename)
    
    # ~ if input_file_extension != '.pdbqt':
        # ~ raise RuntimeError('Input format error. Only PDBQTs ')
    # ~ else:
        # ~ print('Detected .pdbqt')

    if not np:
        np = os.cpu_count() if os.cpu_count() is not None else 1

    ## Create Directories and LogFile
    results_dir = "./results/" + jobname
    create_folder_structure(results_dir)        
    logf = open(results_dir +"/" + jobname + '_fisher.log', 'w+')

    target_list = []

    print('Preparing Ligand and Creating directories...')

    ## Check input format and convert .smi to .mol2
    os.mkdir(results_dir + '/ligands')
    
    print('Checking input format...')

    print(filein)
    if file_extension == '.pdbqt':
        shutil.copyfile(moleculefile, results_dir +"/ligands/" + moleculefilename)
    else:
        ltools.convert_to_pdbqt(moleculefile, results_dir)

    ## Directory Creation

    print('Creating directories...')

    os.mkdir(results_dir + '/docking')
    os.mkdir(results_dir + '/scores')

    if t == 'all':
        print("Defaulting to all available targets")
        selected_targets = [t for t in os.listdir(config.VINA_DOCKING_PATH)]
    else:
        selected_targets = t

    for rec in selected_targets:
        rec_name = pathlib.Path(rec).name
        # print(rec_name)
        os.mkdir(results_dir + '/docking/' + rec_name)
        target_list.append(rec_name)
        pdb = rec_name.split('_')[1]

    dp.generate_run_vina(results_dir, config.VINA_DOCKING_PATH, target_list, jobname, np)

    print('Targets Detected: ')
    print(*target_list, sep = ", ")

    print('Running Autodock Vina')
    result = os.system('sh ' + results_dir + '/docking/run_vina.sh > ' + results_dir + '/docking/vina.log')
    print('Docking finished')

    ### Generate Complexes

    print('Processing Results')
    score_threshold = 1
    rmsd_threshold = 0
    process_results.process_docking_results(jobname, config.VINA_DOCKING_PATH, score_threshold, rmsd_threshold)
    today = date.today()
    pdf_path = f"{results_dir}/{today.strftime('%Y-%m-%d')}_{jobname}.pdf"
    report = PDFReport(jobname, pdf_path)
    report.create_report()

def launch_with_deployed_targets(moleculefile, jobname, np=None, selected_targets=None):
    # Selected targets is a list with names of targets shipped with target fisher
    corrected_targets = []
    for target in selected_targets:
        print(config.VINA_DOCKING_PATH + target)
        if os.path.isdir(config.VINA_DOCKING_PATH + target):
            corrected_targets.append(target)
    print(f"on lwdt for: {selected_targets}")
    print(f"on lwdt for: {corrected_targets}")
    print(moleculefile)
    main(moleculefile=moleculefile, jobname=jobname, np=np, t=corrected_targets)

def fisher_cli():
    if ("-l" in sys.argv) or ("--list-targets" in sys.argv):
        valid_targets  = get_valid_targets()
        print("\t".join(valid_targets))
        exit()

    parser=argparse.ArgumentParser(
        description='''Target Fisher ''',
        epilog=""" """)
    parser.add_argument('moleculefile', type=str, help='Molecules file. Can be .mol2 or .smi')
    parser.add_argument('jobname', type=str, help='Jobname')
    parser.add_argument('-np', type=int, default=0, help='Number of CPUs to use, if null will use all')
    parser.add_argument('-t', type=str, default="all", help='Selected targets', nargs="*", metavar="targets")
    parser.add_argument("-l", '--list-targets', help='List available targets')
    args_parsed = vars(parser.parse_args())
    args_parsed.pop("list_targets")
    main(**args_parsed)

if __name__=='__main__':
    fisher_cli()
