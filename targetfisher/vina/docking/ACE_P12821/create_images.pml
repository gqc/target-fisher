# Color Script
load prep_receptor.mol2;
hide lines;
show cartoon;
set ray_trace_mode, 3; # color
bg_color white;
set antialias, 2;
ray 300,300
png prep_receptor_mol2.png
