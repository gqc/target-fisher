import os
from shutil import make_archive

for f in os.listdir():
	if os.path.isdir(f):
		make_archive(f, 'zip', f)
		print(f, 'ready')
